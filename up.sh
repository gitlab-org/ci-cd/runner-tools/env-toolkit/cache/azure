#!/usr/bin/env bash

NAMESPACE="$1"

terraform init

terraform apply -var "namespace=${NAMESPACE}"

KEY=$(az storage account keys list -g ${NAMESPACE}-rg -n ${NAMESPACE} | jq ".[0].value")

echo "Add the following to your config.toml file"
cat << EOF
  [runners.cache]
    Type = "azure"
    Path = "cache"
    Shared = true
    [runners.cache.azure]
      ServerAddress = "blob.core.windows.net"
      ContainerName = "runner"
      AccountName = "${NAMESPACE}"
      AccountKey = ${KEY}
EOF
