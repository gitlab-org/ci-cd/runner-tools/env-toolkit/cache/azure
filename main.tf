variable "namespace" {
  type = string
}

terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = ">= 2.26"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "runner" {
  name     = "${var.namespace}-rg"
  location = "westus2"
}

resource "azurerm_storage_account" "runner" {
  name                     = var.namespace
  resource_group_name      = azurerm_resource_group.runner.name
  location                 = azurerm_resource_group.runner.location
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = {
    environment = "runner-development"
  }
}

resource "azurerm_storage_container" "runner" {
  name                  = "runner"
  storage_account_name  = azurerm_storage_account.runner.name
  container_access_type = "private"
}
