# Set up Azure cache

Create infrastructure and set up configuration for
[`[runners.cache.azure]`](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runnerscacheazure-section).
Following
https://docs.microsoft.com/en-us/azure/storage/blobs/storage-quickstart-blobs-cli
but with terraform.

## Prerequisites

- [Terraform installed](https://www.terraform.io/downloads.html)
- [Azure CLI installed and logged
  in](https://docs.microsoft.com/en-us/azure/storage/blobs/storage-quickstart-blobs-cli#install-the-azure-cli-locally)

## Run

```shell
# up.sh $NAMESPACE
./up.sh steveazz
```

## Cleanup

```shell
# down.sh $NAMESPACE
./down.sh steveazz
```
