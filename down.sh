#!/usr/bin/env bash

NAMESPACE="$1"

terraform destroy -var "namespace=${NAMESPACE}"
